import React, { useState } from 'react';
import TemperatureInput from './TemperatureInput';
import BoilingVerdict from './BoilingVerdict';
import {toCelsius, toFahrenheit, tryConvert} from './tempStuff';

const Calculator = () => {
  const [scale, setScale] = useState('c');
  const [temperature, setTemperature] = useState(0);
  const celsius = scale === 'f' ? tryConvert(temperature, toCelsius) : temperature;
  const fahrenheit = scale === 'c' ? tryConvert(temperature, toFahrenheit) : temperature;

  const handleCelsiusChange = (temperature) => {
    setScale('c');
    setTemperature(temperature);
  };

  const handleFahrenheitChange = (temperature) => {
    setScale('f');
    setTemperature(temperature);
  }

  return (
    <div>
      <TemperatureInput
        temperature={celsius}
        scale='c'
        onTemperatureChange={handleCelsiusChange} />
      <TemperatureInput
        temperature={fahrenheit}
        scale='f'
        onTemperatureChange={handleFahrenheitChange} />
      <BoilingVerdict
        celsius={parseFloat(celsius)} />
    </div>
  )
}

export default Calculator;